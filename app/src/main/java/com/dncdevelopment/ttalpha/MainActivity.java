package com.dncdevelopment.ttalpha;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Chronometer;
import android.widget.EditText;
import android.widget.ProgressBar;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity
{
    //create variables for the items part of logic
    // and for items on the view
    private Chronometer gameTimeMeter;
    protected ProgressBar gameRoundProgressBar;
    //private TextView promptStartTextView;
    private EditText promptEditText;
    //private TextView promptTextViewOne;
    private EditText promptEditTextOne;
    //private TextView promptTextViewTwo;
    private EditText promptEditTextTwo;
    private FloatingActionButton preFab;
    // logic vars
    private AlertDialog alertDialog;
    private int myCi = 0;
    private String tmpS;
    private String tmpT;
    private String tmpU;
    private ArrayList<String> myArrayListOne = new ArrayList<String>();
    private String myArray[] = {""};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //configure toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //associate variables with id in content view
        //promptStartTextView = (TextView) findViewById(R.id.promptTextView);
        promptEditText = (EditText) findViewById(R.id.promptEditText);
        //promptTextViewOne = (TextView) findViewById(R.id.textViewFirstPlayer);
        promptEditTextOne = (EditText) findViewById(R.id.editTextPOne);
        //hipromptTextViewTwo = (TextView) findViewById(R.id.textViewSecondPlayer);
        promptEditTextTwo = (EditText) findViewById(R.id.editTextPTwo);


        //fab responsible for all 3 text input needed before continuing
        preFab = (FloatingActionButton) findViewById(R.id.fab);
        preFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //put text from all 3 boxes in variables
                tmpS = promptEditText.getText().toString();
                tmpT = promptEditTextOne.getText().toString();
                tmpU = promptEditTextTwo.getText().toString();
                //if all vars have a length then FAB will save values in array
                if ((tmpS.length() > 0) && (tmpT.length() > 0) && (tmpU.length() > 0)) {
                    Log.d("setUpTable::", "All names have value");
                    //prompt user to verify info
                    DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case DialogInterface.BUTTON_POSITIVE:
                                    //Yes button clicked
                                    //save input data into an array to pass to next activity
                                    myArrayListOne.add(tmpS);
                                    myArrayListOne.add(tmpT);
                                    myArrayListOne.add(tmpU);
                                    //myArray[0]= tmpS;
                                    //myArray[2]= tmpU;
                                    //Log.d("alert dia", "myArray is: " + myArray[1]);
                                    //go to next activity to populate gameplayview page two
                                    Intent intent = new Intent("com.dncdevelopment.ttalpha.SecondActivity");
                                    intent.putStringArrayListExtra("player_list", myArrayListOne);
                                    startActivity(intent);
                                    break;
                                case DialogInterface.BUTTON_NEGATIVE:
                                    //No button clicked
                                    promptEditText.setText("");
                                    promptEditTextOne.setText("");
                                    promptEditTextTwo.setText("");
                                    break;
                            }
                        }
                    };

                    AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
                    builder.setMessage("Verify, game name is " + tmpS + ", first player is " + tmpT + " followed by " + tmpU + " ?").setPositiveButton("Yes", dialogClickListener)
                            .setNegativeButton("No", dialogClickListener).show();
                }
            }
        });

    }

    //my Methods
    private void populateListView() {

        }


        @Override
        public boolean onCreateOptionsMenu(Menu menu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            getMenuInflater().inflate(R.menu.menu_main, menu);
            return true;
        }

        @Override
        public boolean onOptionsItemSelected(MenuItem item) {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.
            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_settings) {
                return true;
            }

            return super.onOptionsItemSelected(item);
        }
}
