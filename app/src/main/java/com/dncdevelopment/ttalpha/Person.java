package com.dncdevelopment.ttalpha;

import java.io.Serializable;

/**
 * Created by EMC on 4/19/16.
 */
public class Person implements Serializable{
    private static final long serialVersionUID = 1L;


    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                '}';
    }
}
