package com.dncdevelopment.ttalpha;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.ProgressBar;

import java.util.ArrayList;

public class SecondActivity extends AppCompatActivity{

    private Button atBatButton;
    private Button onDeckButton;
    private Button startStopButton;
    private String tmp0;
    private String tmp1;
    private String tmp2;
    private int myCounti= 0;
    private ProgressBar myProgressBarOne;
    private Chronometer myChronometerOne;
    private ArrayList<String> myArrayListTwo = new ArrayList<String>();
    //private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        atBatButton = (Button) findViewById(R.id.currentPlayerButton);
        onDeckButton = (Button) findViewById(R.id.nextPlayerButton);
        startStopButton = (Button) findViewById(R.id.buttonGameOn);
        myProgressBarOne = (ProgressBar) findViewById(R.id.progressBarOne);
        myChronometerOne = (Chronometer) findViewById(R.id.chronometerOne);
        Intent i = getIntent();
        myArrayListTwo = i.getStringArrayListExtra("player_list");

        String tmp1 = myArrayListTwo.get(1);
        tmp1 = tmp1.toString();
        atBatButton.setText(tmp1);
        tmp2 = myArrayListTwo.get(2);
        tmp2 = tmp2.toString();
        onDeckButton.setText(tmp2);
        String tmp0 = myArrayListTwo.get(0);
        tmp0 = tmp0.toString();
        tmp0 = "Start " + tmp0;
        startStopButton.setText(tmp0);


        //click listeners
        View.OnClickListener ourOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tmp2 = (myArrayListTwo.get(2)).toString();
                atBatButton.setText(tmp2);
                //redisplay list of players
                //populateListView();
            }
        };
        atBatButton.setOnClickListener(ourOnClickListener);

        //Setup Table//
        setUpTable();

        //fab
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //myArrayListTwo.add("bar");
            }
        });
    }


    private void setUpTable() {
        if (myCounti == 0) {
            Log.d("setUpTable", "myCounti = " + myCounti);
            //atBatButton.setVisibility(View.GONE);
            //onDeckButton.setVisibility(View.GONE);
            myProgressBarOne.setVisibility(View.GONE);
            myChronometerOne.setVisibility(View.GONE);

        }
    }

}
